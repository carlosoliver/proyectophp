<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>
        <script src="{$url}public/js/ajaxProduct.js" type="text/javascript"></script>
        
        <!-- SI EXISTEN MAS FICHEROS ASIGNADOS A LA VISTA, LOS CARGARA AUTOMATICAMENTE -->
        
            
    </head>
    <body>
        <div id="header">
            <div id="title">
                Tienda Online <hr>
                {if $log eq 1}
                Usuario: {$usuario} Rango: {$rolName}
              {/if}
            </div>
            {if $idRole ge 3}
            <div  style="float: left">
             <a href="{$url}{$lang}/user" >Lista usuarios</a>
            </div>
            {/if}
            {if $idRole ge 2}
            <div  style="float: left">
             <a href="{$url}{$lang}/product" >Lista productos</a>
            </div>
            {/if}
            <div  style="float: right">
                
          {if $log eq 1}
                <a href="{$url}{$lang}/user/configuration/0" >Mi perfil</a>
                <a href="{$url}{$lang}/login/logout" >Desconectarse</a>
                {else}
                    <a href="{$url}{$lang}/login/register" >Registrarse</a>
                    {/if}
            </div>
        </div>

            
