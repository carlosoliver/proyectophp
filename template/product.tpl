{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('product_list')}</h2>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>{$language->translate('code')}</th>

                <th>{$language->translate('name')}</th>

                <th>{$language->translate('price')}</th>

                <th>{$language->translate('existences')}</th>
                   
                    <th>{$language->translate('operations')}</th>
                   
            </tr>
        </thead>
        {if $idRole ge 3}
        <tfoot>
            <tr><td><input class="newInput" value="Nuevo" type="text" disabled /></td><td><input class="newInput" id="nuevoCodigo" type="text"/></td><td><input class="newInput" id="nuevoNombre" type="text"/></td><td><input class="newInput" id="nuevoPrecio" type="text"/></td><td><input class="newInput" id="nuevoExistencia" type="text"/></td><td id="nuevoProducto"><a href="#"> Nuevo </a></td></tr>
        </tfoot>
        {/if}
        <tbody id="listaProductos">


        </tbody>

    </table>

    <div id="listaPaginas">


    </div>
    {include file="template/footer.tpl" title="footer"}