<?php

require_once 'lib/View.php';

class ProductView extends View
{
    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($template='product.tpl')
    {
//        $this->smarty->assign('method', $this->getMethod());
        
       
        $this->smarty->display($template);
    }
    
}