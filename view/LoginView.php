<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginView
 *
 * @author usuario
 */
require_once 'lib/View.php';

class LoginView extends View
{
    
    
    
    
    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Login<br>';
    }
    
    public function render()
    {
       
        $template='loginForm.tpl';
       if($_SESSION['accessLevel'] == 1){
        $this->smarty->display($template);
       }else{
           unset($_SESSION['comprobarLogin']);
           $template='loginTrue.tpl';
           $this->smarty->display($template);
       }
    }
}
