<?php
abstract class Config{
    const URL = 'http://localhost/proyectophp/';   
    const DBHOST = 'localhost';
    const DBUSER = 'admin';
    const DBPASSWORD = 'admin';
    const DBNAME = 'shop';
    const PAGE_SIZE = 3;
    const DEFAULT_LANG = 'en';
}