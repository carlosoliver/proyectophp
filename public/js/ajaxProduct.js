$(document).ready(function () {


    var nuevoPedido = function (idProd, precioProd, nombreProd) {
        $.post('http://localhost/proyectophp/es/product/ajaxNuevoPedido', {id: idProd, nombre: nombreProd, precio: precioProd}, function (data) {

alert(data);



        }, 'json');
    };

    var newProducto = function (codigo, nombre, precio, existencia) {

        $.post('http://localhost/proyectophp/es/product/ajaxNewProduct', {codigoProd: codigo, nombreProd: nombre, precioProd: precio, existenciaProd: existencia}, function (data) {
            if (data) {

                alert("PRODUCTO insertado con exito");

            }


        }, 'json');
    };

    var deleteProducto = function (idDelete) {

        $.post('http://localhost/proyectophp/es/product/ajaxDeleteProduct/' + idDelete, "", function (data) {

            if (data) {
                $("#row" + idDelete).empty();
            } else {
                alert("Borrado no realizado con exito");
            }

        }, 'json');

    };
    var updateProducto = function (codigoInfo, tipoUpdate, idProd) {

        $.post('http://localhost/proyectophp/es/product/ajaxUpdateProduct', {id: idProd, campo: tipoUpdate, value: codigoInfo}, function (data) {


        }, 'json');

    };

    var loadPaginas = function () {

        $.post('http://localhost/proyectophp/es/product/ajaxGetNumPages', "", function (data) {
            $("#listaPaginas").html('');
            var paginas = "";


            for (var pagina = 1; pagina <= data; pagina++) {

                paginas += '<a href="#"><div id="' + pagina + '" class="paginacion">' + pagina + '</div></a>  ';


            }


            $("#listaPaginas").append(paginas);
        }, 'json');
    };


    var loadProductos = function (pagina) {

        $.post('http://localhost/proyectophp/es/product/ajaxGetPage/' + pagina, "", function (data) {

            var tabla = "";
            var paginas = "";


            $("#listaProductos").html('');

            for (var i = 0; i < data[0].length; i++) {

                //  tabla += '<tr id="row' + data[1][i].id + '"><td>' + data[1][i].id + '</td><td><input id="codigo' + data[1][i].id + '" type="text" oldValue="' + data[1][i].codigo + '" value="' + data[1][i].codigo + '" /></td><td><input oldValue="' + data[1][i].nombre + '" id="nombre' + data[1][i].id + '" type="text" value="' + data[1][i].nombre + '" /></td><td><input oldValue="' + data[1][i].precio + '" id="precio' + data[1][i].id + '" type="text" value="' + data[1][i].precio + '" /></td><td><input oldValue="' + data[1][i].existencia + '" id="existencia' + data[1][i].id + '" type="text" value="' + data[1][i].existencia + '" /></td>';
                tabla += '<tr id="row' + data[0][i].id + '"><td>' + data[0][i].id + '</td><td><input class="inputUpdate" name="codigo" type="text" oldValue="' + data[0][i].codigo + '" value="' + data[0][i].codigo + '" /></td><td><input oldValue="' + data[0][i].nombre + '" class="inputUpdate" name="nombre" type="text" value="' + data[0][i].nombre + '" /></td><td><input oldValue="' + data[0][i].precio + '" class="inputUpdate" name="precio" type="text" value="' + data[0][i].precio + '" /></td><td><input oldValue="' + data[0][i].existencia + '" class="inputUpdate" name="existencia" type="text" value="' + data[0][i].existencia + '" /></td>';
                tabla += '<td >';
                if (data[1] >= 3) {
                    tabla += '<a href="#" id="delete' + data[0][i].id + '"> Borrar</a>';
                }
                tabla += '<a href="#" class="pedido" ide="' + data[0][i].id + '" precio="' + data[0][i].precio + '" nombre="' + data[0][i].nombre + '"> Comprar</a>';
                tabla += '</td>';
                tabla += '</tr>';




                $(document).on("click", '#delete' + data[0][i].id, function () {
                    var idProd = $(this).attr("id");

                    deleteProducto(idProd.substring(6))


                });

                if (data[1] >= 3) {


                    $(document).on("focusout", '.inputUpdate', function () {
                        var idProd = $(this).parent().parent().attr('id');
                        idProd = idProd.substring(3);
                        var codigoInfo = this.value;
                        var comprueba = $(this).attr('oldValue');

                        var tipoUpdate = this.name;
                        if (comprueba === codigoInfo) {


                        } else {
                            updateProducto(codigoInfo, tipoUpdate, idProd);
                            $(this).attr("oldValue", codigoInfo);
                            alert($(this).attr("oldValue"));
                        }



                    });


                }//FIN IF

            } //FIN FOR







            $("#listaProductos").append(tabla);

        }, 'json');

    };


    $(document).on("click", '.pedido', function (e) {
        e.preventDefault();
        var idProd = $(this).attr("ide");
        var precio = $(this).attr("precio");
        var nombre = $(this).attr("nombre");
        
        nuevoPedido(idProd, precio, nombre);
    });

    $(document).on("click", '#nuevoProducto', function (e) {
        e.preventDefault();
        var codigoProd = $('#nuevoCodigo').attr('value');
        var nombreProd = $('#nuevoNombre').attr('value');
        var precioProd = $('#nuevoPrecio').attr('value');
        var existenciaProd = $('#nuevoExistencia').attr('value');


        newProducto(codigoProd, nombreProd, precioProd, existenciaProd);

        $('#nuevoCodigo').removeAttr('value');
        $('#nuevoNombre').removeAttr('value');
        ;
        $('#nuevoPrecio').removeAttr('value');
        $('#nuevoExistencia').removeAttr('value');
    });

    $(document).on("click", '.paginacion', function () {
        var idPagina = $(this).attr("id");


        loadProductos(idPagina);

    });



    loadPaginas();
    loadProductos(1);
});




