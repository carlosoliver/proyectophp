<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author usuario
 */
require_once 'lib/Controller.php';



class Login extends Controller{
   

    function __construct()
    {
        parent::__construct('Login');
        
    }
    
    function index(){
        
        $this->view->render();
        
    }
    
    function access(){
        
        $row = $_POST;  
        $row['password'] = md5($row['password']);
      
        
        $loginRow=$this->model->check($row);   
        
       
       
       if(isset($loginRow[0]['idRole'])){
          
        $_SESSION['accessLevel'] = $loginRow[0]['idRole'];
        $_SESSION['nombreUsu'] = $loginRow[0]['nombre'];
        $_SESSION['nombreRole'] = $loginRow[0]['role'];
        $_SESSION['idUsuario'] = $loginRow[0]['id'];
       $_SESSION['logOut'] = 1;
       
       
       
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/product');
      
       }else{
           
           
           
       header('Location: ' . Config::URL . $_SESSION['lang'] . '/index');
     
       }
    }
    
    
    function logout(){
       
        session_destroy();
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/index');
        
        unset($_SESSION);
        
    }
    private function _validate($row)
    {
        $error = array();
               
        if (!preg_match("/^.{6,20}$/", $row['password'])){
            $error['password'] = 'error_password';
        }
        
        return $error;
    }
    
    
}
