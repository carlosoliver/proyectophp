<?php
require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Product extends Controller{
  

    function __construct()
    {
        parent::__construct('Product');
    }

   
    public function index()
    {
        //mostrar lista de todos los registros.
       
        $this->view->render();
    }
    public function ajaxGetNumPages()
    {
        $num = $this->model->getNumPages();
      echo json_encode($num);
        
    }
    
    
    public function ajaxGetPage($pagina)
    {
        $rows = $this->model->getPage($pagina);
      echo json_encode($rows);
      
        
        
    }
    public function ajaxDeleteProduct($idProduct)
    {
        
      $option=$this->model->deleteProduct($idProduct);
      echo json_encode($option);
          
    }
    public function ajaxNewProduct()
    {
        $codigo = $_POST['codigoProd'];
        $nombre = $_POST['nombreProd'];
        $precio = $_POST['precioProd'];
        $existencia = $_POST['existenciaProd'];
        
      $option=$this->model->addProduct($codigo, $nombre, $precio, $existencia);
      echo json_encode($option);
          
    }
    public function ajaxUpdateProduct()
    {
       $idProduct = $_POST['id'];
       $info = $_POST['value'];
       $tipoUpdate = $_POST['campo'];
       
      $option=$this->model->updateProduct($idProduct, $info, $tipoUpdate);
      echo json_encode($option);
        
    }
    
    public function ajaxNuevoPedido()
    {
        $pedido['id'] = $_POST['id'];
       $pedido['nombre'] = $_POST['nombre'];
       $pedido['precio'] = $_POST['precio'];
       
      $_SESSION['listaPedido'][$pedido['id']] = $pedido;
      echo json_encode($_SESSION['listaPedido']);
      
    }
    
}